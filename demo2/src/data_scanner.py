def search_asset_by_status(items, asset_status='Pending Return'):
    result = []
    return [result.append(item) for item in items if item.get("asset_status") == asset_status]


def search_by_token(item_lower, search_tokens):
    is_token_in_item = []
    for token in search_tokens:
        is_token_in_item.append(token.lower() in item_lower)
    return all(is_token_in_item)


def search_asset_by_technical_specs(items, search_tokens):
    if len(search_tokens) == 0:
        return []
    list_founded = []
    for item in items:
        item_lower = list(map(lambda x: x.lower(), item.get("technical_specs").split()))
        if search_by_token(item_lower, search_tokens):
            list_founded.append(
                {
                    "serial_no": item["serial_no"],
                    "asset_type": item["asset_type"],
                    "hardware_standard": item["hardware_standard"],
                    "technical_specs": item["technical_specs"],
                    "asset_status": item["asset_status"]
                })
    return list_founded

