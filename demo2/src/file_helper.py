import os


def search_files(path, extension):
    output = []
    for root, dirs, files in os.walk(path):
        files_full_name = [os.path.join(root, f) for f in files if f.endswith(extension)]
        output.extend(files_full_name)
    return output


def read_data(files):
    data_list = []
    try:
        for file in files:
            file_opened = open(file, 'r')
            data = file_opened.readlines()
            data_list.append(data)
            file_opened.close()
    except FileNotFoundError:
        return []
    else:
        return data_list
    