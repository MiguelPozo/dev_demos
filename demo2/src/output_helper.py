def write_txt_file(data, output_path):
    if not isinstance(data, str):
        return {'statusCode': 'ok', 'body': None}
    else:
        file = open(output_path, 'w')
        file.write(data)
        file.close()
