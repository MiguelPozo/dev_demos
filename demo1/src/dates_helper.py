from datetime import datetime, timedelta


def order_date(str_date):
    list_str = str_date.split("-")
    return(f"{int(list_str[1])}/{int(list_str[2])}/{list_str[0]}")  #returns mm/dd/YYYY


def get_dates_in_interval(start_date, end_date):
    result = []
    if start_date is None or end_date is None:
        return
    if start_date == end_date:
        return [start_date]

    # TODO: Solve normal use case
    start_date = datetime.strptime(start_date, "%m/%d/%Y")
    end_date = datetime.strptime(end_date, "%m/%d/%Y")
    if end_date < start_date:
        return 
    elapsed_days = end_date - start_date
    result.append(order_date(str(start_date.date())))
    for _ in range(elapsed_days.days):
        start_date = start_date + timedelta(days = 1)
        ordered_date = order_date(str(start_date.date()))
        result.append(ordered_date)
    return result
